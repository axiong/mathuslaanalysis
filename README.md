# TestStand Analysis Template

[![pipeline status](https://gitlab.cern.ch/mathusla/TestStandAnalysisTemplate/badges/master/pipeline.svg)](https://gitlab.cern.ch/mathusla/TestStandAnalysisTemplate/commits/master)

This repository contains a few starter scripts and templates to setup a small easy to use program that will run through mathulsa binary files.
The below instructions show you how to clone, create a small analysis loop, and push the resulting code into your own repository.

For documentation using this template repro and on the library, see the
[TestStandOffline library documentation for version 1.3](https://teststandoffline.readthedocs.io/en/version-1.3/).

### Build and Install

Follow the below steps to start a analysis repro from scratch. It checks out the template repro, builds a simple demo, and then pushes it to the new repro. 

1. As a prerequisit we need to make sure we are running on an advanced enough machine.
We need gcc 5.x and also CMAKE 3.3 or better, and a modern version of ROOT if you want to do any plotting. If you have the ATLAS environment availible to you, then you can do the following:
    ```
    setupATLAS
    lsetup cmake
    lsetup root
    ```

1. First we will get the template onto our local disk.

    ```
    git clone https://:@gitlab.cern.ch:8443/mathusla/TestStandAnalysisTemplate.git myanalysis
    cd myanalysis
    ```

1. Next we will create a small executable program and build it.
    ```
    ./scripts/make_CMakeList
    ./scripts/add_executable myfirst
    ./install
    cd build
    ```

1. Now everything is setup and you are ready to work. The edit, build, run loop looks like this (where vi is a text editor):
    ```
    vi ../src/myfirst.cpp
    make
    ./myfirst <mathusla-filename>
    ```

1. When you are ready to push this source to source control you can use the following to tell git to push to your repro, not the template one (please don't!!). For this we need an empty repository (called `myanalysis` for this analysis).
```
    git remote set-url origin https://:@gitlab.cern.ch:8443/gwatts/myanalysis.git
    git push --all
```

1. Don't forget to check in your changes:
    ```
    git status
    git add CMakeLists.txt src/myfirst.cpp
    git commit -m "First go at new files"
    git push
    ```

Some extra notes:

The `install` command pulls down the `TestStandOffline` repository. The `install` command takes one optional argument, 
`[ssh|http|https|krb5]` which determines how gitlab is accessed to pull down the `TestStandOffline` repository. The default is `krb5`.

If you don't want or don't have ROOT, remove the lines from the CMakeList.txt file.

### Changing the version

The `TestStandOffline` is periodically updated. You can see all the release on the [tags page](https://gitlab.cern.ch/mathusla/TestStandOffline/tags) of that repository.

To change to a new version of the offline libarary just do the following:

1. Delete the `build` directory in your library.

1. Edit the `install` script and search for the line that starts with `git checkout`. Change the version number to the tag you are iterested in (e.g. 1.4).

1. Execute `./install` to pull down the new version.

### Editing Template

The `add_executable` command will add template analysis programs in the src directory. The editing should occur there.
You can see some coding examples in the [tests directory in the TestStandOffline library](https://gitlab.cern.ch/mathusla/TestStandOffline/tree/af61e055e50db58bc3ef2a88f50f52e6df5cb5b3/test). You can see all the data that is availible by looking at the [MEvent](https://gitlab.cern.ch/mathusla/TestStandOffline/blob/master/TestStandOffline/MEvent.hpp).
